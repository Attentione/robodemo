*** Settings ***
Library         DatabaseLibrary
Library         RPA.FileSystem



*** Variables ***
${DBHost}           127.0.0.1                                           #Hosts IP address
${DBport}           5432
${DBName}           Robosql_db
${DBUser}           postgres
${DBPassword}       robosql
${ExcelDir}         ../Clients                                          #Path to directory where the data is located. Can be absolute or relative path
${DBTable}          public.accounts





*** Keywords ***

Connect to Database
    Connect to Database Using Custom Params     psycopg2    database='${DBName}' , user='${DBUser}' , password='${DBPassword}' , host='${DBHost}' , port='${DBport}'

Export Files to Database
    ${files}=       List files in directory     ${ExcelDir}
    FOR     ${file}     IN      @{FILES}
        ${output}=      Execute Sql String  COPY ${DBTable} (customerkey,firstname,lastname,emailaddress,phone) FROM '${file}' DELIMITER ',' CSV HEADER;
        Log     ${output}
    END
    Disconnect From Database









